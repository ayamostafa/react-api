
import React, { useState, useEffect } from "react";
import axios from "../axios";
import Product from "./Product";
import './App.css';

function App() {

  const [products, setProducts] = useState([{ id: 1, name: "test" }]);
  const [loading, setLoading] = useState(false);

  //Load Data when Component Load
  useEffect(() => {
    setLoading(true);
    axios.get("user/products").then((res) => {
      setLoading(false);

      console.log(res)
    }).catch((err) => {
      setLoading(false);

      console.log(err);
    });
  }, []);
  return (
    <div className="App">
      {loading ? "Loading...." : products.map((product) => {
        return <Product key={product.id} data={product} />
      })}
    </div>
  );
}

export default App;
